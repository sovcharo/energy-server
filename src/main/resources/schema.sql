CREATE SCHEMA IF NOT EXISTS energy;
CREATE TABLE IF NOT EXISTS energy.company_type (
	id SERIAL primary key,
	company_type_name VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS energy.product_type (
	id SERIAL primary key,
	type_name VARCHAR(255)
);
create extension IF NOT EXISTS hstore;
create table IF NOT EXISTS energy.calculation (
id SERIAL primary key,
created_on TIMESTAMP,
company_type INT,
product_base_prices hstore
);

create table if not exists energy.price_per_quarter (
	id SERIAL primary key,
	q int not null,
	year int not null,
	energy_price VARCHAR(255) not null,
	lgc_price VARCHAR(255) not null
);

create table if not exists energy.load_shape_premium (
	id SERIAL primary key,
	price_per_quarter_id int not null,
	company_type int not null,
	load_shape_premium float(4) not null,
	CONSTRAINT fk_lsp_prices
   	FOREIGN KEY(price_per_quarter_id)
   	REFERENCES energy.price_per_quarter(id)
 )