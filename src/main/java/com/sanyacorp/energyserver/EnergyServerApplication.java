package com.sanyacorp.energyserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnergyServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnergyServerApplication.class, args);
	}

}
