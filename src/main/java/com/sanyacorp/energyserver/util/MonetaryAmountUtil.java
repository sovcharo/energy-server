package com.sanyacorp.energyserver.util;

import javax.money.Monetary;
import javax.money.MonetaryAmount;

public class MonetaryAmountUtil {

    public enum Currency {
        USD
    }

    public static MonetaryAmount of(Number number, Currency currency) {
        return Monetary
                .getDefaultAmountFactory()
                .setCurrency(currency.name())
                .setNumber(number.longValue())
                .create();
    }

    public static MonetaryAmount of(Number number) {
        return Monetary
                .getDefaultAmountFactory()
                .setCurrency(Currency.USD.name())
                .setNumber(number.longValue())
                .create();
    }
}
