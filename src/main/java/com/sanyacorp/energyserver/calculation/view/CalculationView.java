package com.sanyacorp.energyserver.calculation.view;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sanyacorp.energyserver.calculation.model.Calculation;
import com.sanyacorp.energyserver.calculation.model.CompanyType;
import com.sanyacorp.energyserver.calculation.model.ProductType;
import com.sanyacorp.energyserver.util.MonetaryAmountUtil;
import lombok.*;

import javax.money.MonetaryAmount;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CalculationView {

    private LocalDateTime createdOn;
    private CompanyType companyType;
    private Map<ProductType, MonetaryAmount> productBasePrices = new HashMap<>();

    @JsonProperty
    public MonetaryAmount getTotal() {

        return productBasePrices
                .values()
                .stream()
                .reduce(MonetaryAmountUtil.of(0), MonetaryAmount::add);
    }

    public CalculationView(Calculation calculation) {
        if(calculation == null) {
            return;
        }
        createdOn = calculation.getCreatedOn();
        companyType = calculation.getCompanyType();
        productBasePrices = calculation.getProductBasePrices();
    }

}
