package com.sanyacorp.energyserver.calculation.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.money.MonetaryAmount;
import javax.persistence.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
// Hibernate issue https://hibernate.atlassian.net/browse/HHH-12126
// @Table(name = "calculation", schema= "energy")
@org.springframework.data.relational.core.mapping.Table("energy.calculation")
public class Calculation {

    @Id
    private Integer id;

    @Column(name="created_on")
    private LocalDateTime createdOn;

    @Column(name="company_type")
    @Enumerated(EnumType.ORDINAL)
    private CompanyType companyType;

    @Column(name="product_base_prices")
    @Type(type="hstore")
    private Map<ProductType, MonetaryAmount> productBasePrices = new HashMap<>();
}
