package com.sanyacorp.energyserver.calculation.model;

public enum ProductType {
    ENERGY,
    LGC
}
