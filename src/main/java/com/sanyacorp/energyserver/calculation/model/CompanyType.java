package com.sanyacorp.energyserver.calculation.model;

public enum CompanyType {
    MINING,
    INDUSTRIAL,
    COMMERCIAL
}

