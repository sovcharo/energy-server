package com.sanyacorp.energyserver.calculation.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table("energy.load_shape_premium")
@Entity
public class LoadShapePremium {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name="company_type")
    private CompanyType companyType;

    @Column(name="load_shape_premium")
    private Float loadShapePremium;
}
