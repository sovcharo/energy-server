package com.sanyacorp.energyserver.calculation.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CollectionId;
import org.springframework.data.relational.core.mapping.Table;

import javax.money.MonetaryAmount;
import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table("energy.price_per_quarter")
@Entity
public class PricePerQuarter {

    @Id
    @GeneratedValue
    private Integer id;

    private int q;
    private int year;

    @Column(name="energy_price")
    private MonetaryAmount energy_price;

    @Column(name="lgc_price")
    private MonetaryAmount lgc_price;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    @JoinColumn(name = "price_per_quarter_id")
    private List<LoadShapePremium> loadShapePremiumList;
}
