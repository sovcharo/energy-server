package com.sanyacorp.energyserver.calculation.repository;

import com.sanyacorp.energyserver.calculation.model.Calculation;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface CalculationRepository extends ReactiveSortingRepository<Calculation, Long> {
    // workaround for the fact that ReactiveRepository does not support findAll(Pageable pageable)
    Flux<Calculation> findByIdNotNull(Pageable pageable);
}
