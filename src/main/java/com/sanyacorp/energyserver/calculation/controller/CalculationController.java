package com.sanyacorp.energyserver.calculation.controller;

import com.sanyacorp.energyserver.calculation.model.Calculation;
import com.sanyacorp.energyserver.calculation.model.CompanyType;
import com.sanyacorp.energyserver.calculation.model.ProductType;
import com.sanyacorp.energyserver.calculation.service.PriceCalculationService;
import com.sanyacorp.energyserver.calculation.view.CalculationView;
import com.sanyacorp.energyserver.exceptions.MissingCompanyTypeException;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Api
@RestController
@RequestMapping("/calculations")
public class CalculationController {

    private PriceCalculationService priceCalculationService;

    @Autowired
    CalculationController(PriceCalculationService priceCalculationService) {
        this.priceCalculationService = priceCalculationService;
    }

    @GetMapping
    public Flux<CalculationView> listAll(@RequestParam(name = "page") int page,
                                         @RequestParam(name = "size") int size) {
        return priceCalculationService.listAll(PageRequest.of(page, size)).flatMap(calculation -> Mono.just(new CalculationView(calculation)));
    }

    @PostMapping
    public Mono<CalculationView> calculate(@RequestParam LocalDate startDate,
                                       @RequestParam LocalDate endDate,
                                       @RequestParam String companyType,
                                       @RequestParam List<String> productTypes) {

        if(companyType == null) {
            throw  new MissingCompanyTypeException();
        }

        Mono<Calculation> calculationMono = priceCalculationService.calculate(
                startDate,
                endDate,
                CompanyType.valueOf(companyType),
                productTypes.stream().map(ProductType::valueOf).collect(Collectors.toSet()));

        return Mono.just(new CalculationView(calculationMono.block()));
    }
}
