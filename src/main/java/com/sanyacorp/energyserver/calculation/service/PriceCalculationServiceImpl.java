package com.sanyacorp.energyserver.calculation.service;

import com.sanyacorp.energyserver.calculation.model.Calculation;
import com.sanyacorp.energyserver.calculation.model.CompanyType;
import com.sanyacorp.energyserver.calculation.model.ProductType;
import com.sanyacorp.energyserver.calculation.repository.CalculationRepository;
import com.sanyacorp.energyserver.util.MonetaryAmountUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.money.MonetaryAmount;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PriceCalculationServiceImpl implements PriceCalculationService {

    private CalculationRepository calculationRepository;

    @Autowired
    PriceCalculationServiceImpl(CalculationRepository calculationRepository) {
        this.calculationRepository = calculationRepository;
    }

    public Mono<Calculation> calculate(LocalDate startDate, LocalDate endDate, CompanyType companyType, Set<ProductType> productTypes) {

        Calculation calculation = new Calculation();
        calculation.setCreatedOn(LocalDateTime.now());
        calculation.setCompanyType(companyType);
        calculation.setProductBasePrices(
                productTypes
                        .stream()
                        .collect(Collectors.toMap(type->type, type -> getProductBasePrice(startDate, endDate, companyType, type))));
        return calculationRepository.save(calculation);
    }

    @Override
    public Flux<Calculation> listAll(Pageable pageable) {
        return calculationRepository.findByIdNotNull(pageable);
    }

    private MonetaryAmount getProductBasePrice(LocalDate startDate, LocalDate endDate, CompanyType companyType, ProductType productType) {
        List<MonetaryAmount> quarterlyPrices = getProductQuarterlyPrices(startDate, endDate, companyType, productType);
        if(CollectionUtils.isEmpty(quarterlyPrices)) return MonetaryAmountUtil.of(0);
        return quarterlyPrices.stream().reduce( MonetaryAmountUtil.of(0), MonetaryAmount::add).divide(quarterlyPrices.size());
    }

    private List<MonetaryAmount> getProductQuarterlyPrices(LocalDate startDate, LocalDate endDate, CompanyType companyType, ProductType productType) {
        return null;
    }

    private int getQuarter(LocalDate date) {
        return Math.round(date.getMonthValue()/3f);
    }
}
