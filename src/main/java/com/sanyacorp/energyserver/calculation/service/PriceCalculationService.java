package com.sanyacorp.energyserver.calculation.service;

import com.sanyacorp.energyserver.calculation.model.Calculation;
import com.sanyacorp.energyserver.calculation.model.CompanyType;
import com.sanyacorp.energyserver.calculation.model.ProductType;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.Set;

public interface PriceCalculationService {
    Mono<Calculation> calculate(LocalDate startDate, LocalDate endDate, CompanyType companyType, Set<ProductType> productTypes);

    Flux<Calculation> listAll(Pageable pageable);
}
