package com.sanyacorp.energyserver.exceptions;

public abstract class HighVoltageException extends RuntimeException {
    public abstract int getErrorCode();
}
