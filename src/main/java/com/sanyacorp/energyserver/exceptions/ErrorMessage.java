package com.sanyacorp.energyserver.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@AllArgsConstructor
public class ErrorMessage {

    private HttpStatus status;
    private String exception;
    private int errorCode;
    private String userMessage;

}
