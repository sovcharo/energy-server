package com.sanyacorp.energyserver.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

// TODO provide the messages in a property file
// TODO  Have a constants list for the error codes
@ControllerAdvice
public class HighVoltageExceptionHandler {

    @ExceptionHandler(MissingCompanyTypeException.class)
    ResponseEntity<ErrorMessage> handleMissingCompanyType(MissingCompanyTypeException ex) {

        return getErrorMessage(HttpStatus.BAD_REQUEST, ex, "No company type provided.");
    }

    @ExceptionHandler(InvalidPeriodException.class)
    ResponseEntity<ErrorMessage> handleInvalidPeriod(InvalidPeriodException ex) {

        return getErrorMessage(HttpStatus.BAD_REQUEST, ex, "No information available for the provided period.");
    }

    private ResponseEntity<ErrorMessage> getErrorMessage(HttpStatus status, HighVoltageException ex, String message) {
        ErrorMessage errorMessage = new ErrorMessage(
                status,
                ex.toString(),
                ex.getErrorCode(),
                "No information available for the provided period.");

        return new ResponseEntity<>(errorMessage, status);
    }
}

