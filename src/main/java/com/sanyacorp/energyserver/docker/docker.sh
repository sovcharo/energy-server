#!/bin/bash
docker create -v /var/lib/postgresql/data --name PostgresData alpine
docker run -p 5432:5432 --name high-voltage -e POSTGRES_PASSWORD=energy -d --volumes-from PostgresData postgres